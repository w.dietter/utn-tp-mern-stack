#### Trabajo Final realizado para el curso de desarrollo web avanzado dictado en la UTN.

Aplicación web realizada para una red de atención en salud mental.
Permite realizar operaciones de CRUD sobre pacientes y profesionales. Los Pacientes pueden realizar 
solicitudes de turnos que serán registradas por los profesionales.
Posee autenticación y autorización basada en roles.

Tecnologías:
+ ReactJs
+ Redux
+ Authentication/Authorization/JWT
+ NodeJs
+ ExpressJs
+ MongoDb / Mongoose
+ Axios
+ React-toastify
+ Sass

```
npm run dev
```