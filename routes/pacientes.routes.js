const express = require('express');
const pacientesController = require('../controllers/pacientes.controller');
const auth = require('../middleware/auth');

const router = express.Router();

router
  .route('/')

  /*@route 			GET			api/pacientes
    @desc 			Get all pacientes
    @access 		PRIVATE		*/
  .get(auth, pacientesController.getPacientes)

  /*@route 			POST			api/pacientes
    @desc 			create new paciente
    @access 		PRIVATE		*/
  .post(auth, pacientesController.createPaciente);

router
  .route('/solicitudes')

  /*@route 			GET			api/pacientes/solicitudes
    @desc 			Get solicitudes de turno
    @access 		PRIVATE		*/
  .get(auth, pacientesController.getSolicitudes);

router
  .route('/solicitudes/:id')

  /*@route 			DELETE			api/pacientes/solicitudes/:id
    @desc 			Delete solicitud de turno
    @access 		PRIVATE		*/
  .delete(auth, pacientesController.deleteSolicitudPaciente);

router
  .route('/:id')

  /*@route 			GET			api/pacientes/:id
    @desc 			Get paciente by id
    @access 		PRIVATE		*/
  .get(auth, pacientesController.getPacienteById)

  /*@route 			DELETE			api/pacientes/:id
    @desc 			Delete paciente
    @access 		PRIVATE		*/
  .delete(auth, pacientesController.deletePaciente)

  /*@route 			PUT			api/pacientes/:id
    @desc 			update paciente
    @access 		PRIVATE		*/
  .put(auth, pacientesController.updatePaciente);

router
  .route('/solicitarturno')

  /*@route 			POST			api/pacientes/solicitarturno
    @desc 			Crear un nuevo paciente que solicita turno y no tiene profesional asignado
    @access 		PUBLIC		*/
  .post(pacientesController.solicitarTurno);

module.exports = router;
