const express = require('express');
const auth = require('../middleware/auth');

const router = express.Router();
const usersController = require('../controllers/users.controller');

router
  .route('/')
  /*@route 			POST			api/users
  @desc 			Register user and return token
  @access 		Public		*/
  .post(usersController.registerUser)
  /*@route 			GET			api/users
    @desc 			Get all users
    @access 		PRIVATE		*/
  .get(auth, usersController.getUsers);

router
  .route('/solicitudes')
  /*@route 			GET			api/users/solicitudes
    @desc 			GET solicitudes -> users con isValidatedByAdmin = false
    @access 		PRIVATE		*/
  .get(usersController.getSolicitudes);

router
  .route('/solicitudes/:id')
  /*@route 			PUT			api/users/solicitudes/:id
    @desc 			Aprobar solicitud -> cambiar isValidatedByAdmin a true
    @access 		PRIVATE		*/
  .put(auth, usersController.aprobarSolicitud)
  .delete(auth, usersController.deleteSolicitud);

module.exports = router;
