const express = require('express');
const authControllers = require('../controllers/auth.controller');
const auth = require('../middleware/auth');

const router = express.Router();

router
  .route('/')

  /*@route 			GET			/api/auth/
  @desc 			Get auth user providing the token -> check if the token is valid ('x-access-token').
  @access 		PRIVATE		*/
  .get(auth, authControllers.authenticateUser)

  /*@route 			POST			/api/auth/
  @desc 			Authenticate user and get token -> Log in
  @access 		PUBLIC		*/
  .post(authControllers.loginUser);

module.exports = router;
