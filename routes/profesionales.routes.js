const express = require('express');
const profesionalesController = require('../controllers/profesionales.controller');
const auth = require('../middleware/auth');

const router = express.Router();

router
  .route('/')

  /*@route 			GET			api/profesionales
    @desc 			Get all profesionales
    @access 		PRIVATE		*/
  .get(auth, profesionalesController.getProfesionales)

  /*@route 			POST			api/profesionales
    @desc 			Create a new profesional
    @access 		PRIVATE		*/
  .post(auth, profesionalesController.createProfesional);

router
  .route('/:id')

  /*@route 			GET			api/profesionales/:id
    @desc 			Get profesional by id
    @access 		PRIVATE		*/
  .get(auth, profesionalesController.getProfesionalByUserId)

  /*@route 			DELETE			api/profesionales/:id
      @desc 			Delete a profesional by id
      @access 		PRIVATE		*/
  .delete(auth, profesionalesController.deleteProfesional)

  /*@route 			PUT			api/profesionales/:id
      @desc 			Update a profesional
      @access 		PRIVATE		*/
  .put(auth, profesionalesController.updateProfesional);

router
  .route('/:profesionalId/pacientes/:pacienteId')

  /*@route 			PUT			api/profesionales/:id/pacientes/:pacienteId
    @desc 			Add paciente to profesional
    @access 		PRIVATE		*/
  .put(auth, profesionalesController.addPacienteToProfesional);

module.exports = router;
