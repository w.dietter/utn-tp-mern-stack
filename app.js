const express = require('express');
const logger = require('morgan');
const connectToDb = require('./config/db');
const createAdminUser = require('./utils/createAdminUser');

const app = express();
connectToDb();
createAdminUser();

/* middleware */
app.use(logger('dev'));
app.use(express.json());

app.use('/api/auth', require('./routes/auth.routes'));
app.use('/api/users', require('./routes/users.routes.js'));
app.use('/api/profesionales', require('./routes/profesionales.routes'));
app.use('/api/pacientes', require('./routes/pacientes.routes'));

module.exports = app;
