const jwt = require('jsonwebtoken');

module.exports = async function generateAuthToken(
  payload,
  secret
) {
  return await jwt.sign(payload, secret, {
    expiresIn: 36000
  });
};
