const bcrypt = require('bcryptjs');
const User = require('../models/User.model');

async function createAdminUser() {
  try {
    const user = new User({
      name: 'Admin',
      role: 'admin',
      isValidatedByAdmin: true,
      email: 'admin',
      password: 'asdasd'
    });

    user.password = await bcrypt.hash(user.password, 10);
    await user.save();
  } catch (error) {
    console.log('Admin user created');
  }
}

module.exports = createAdminUser;
