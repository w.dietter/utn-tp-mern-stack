const mongoose = require('mongoose');
const User = require('./User.model');
const Paciente = require('./Paciente.model');

const ProfesionalSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    default: null
  },
  nombre: {
    type: String,
    required: true
  },
  apellido: {
    type: String,
    required: true
  },
  especialidad: {
    type: String,
    required: true,
    lowercase: true
  },
  nroMatricula: {
    type: Number,
    required: true,
    unique: true
  },
  pacientes: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'paciente'
  }
});

ProfesionalSchema.statics.getNotValidatedByAdmin = async function() {
  const profesionales = await this.find().populate('user');
  return profesionales.filter(
    profesional => profesional.user.isValidatedByAdmin === false
  );
};

ProfesionalSchema.pre('remove', async function(next) {
  const profesionalId = this._id;
  const userId = this.user;
  if (userId) {
    await User.findByIdAndDelete(userId);
  }
  const pacientes = await Paciente.find({ profesional: profesionalId });
  pacientes.forEach(async paciente => {
    paciente.profesional = null;
    await paciente.save();
  });
  next();
});

module.exports = mongoose.model(
  'profesional',
  ProfesionalSchema,
  'profesionales'
);
