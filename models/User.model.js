const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    trim: true
  },
  role: {
    type: String,
    enum: ['profesional', 'admin'],
    required: true,
    default: 'profesional',
    lowercase: true
  },
  isValidatedByAdmin: {
    type: Boolean,
    required: true,
    default: false
  }
});

UserSchema.statics.hashPassword = async function(password) {
  return await bcrypt.hash(password, 10);
};

module.exports = mongoose.model('user', UserSchema);
