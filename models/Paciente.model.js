const mongoose = require('mongoose');

const PacienteSchema = mongoose.Schema({
  profesional: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'profesional',
    default: null
  },
  nombre: {
    type: String,
    required: true
  },
  apellido: {
    type: String,
    required: true
  },
  dni: {
    type: Number,
    required: true,
    unique: true
  },
  edad: {
    type: Number,
    required: true,
    min: [18, 'El paciente debe ser mayor de edad']
  },
  diagnostico: {
    type: String
  },
  obraSocial: {
    type: String
  },
  telefono: {
    type: String
  },
  email: {
    type: String
  }
});

module.exports = mongoose.model('paciente', PacienteSchema);
