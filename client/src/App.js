import React from 'react';
import './styles/main.scss';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

/* utils */
import setAuthToken from './utils/sethAuthToken';

/* redux */
import { connect } from 'react-redux';
import { loadUser } from './redux/auth/auth.actions';

/* components */
import Navbar from './components/Navbar/Navbar.component';
import Home from './components/pages/Home/Home.component';
import Dashboard from './components/pages/Dashboard/Dashboard.component';
import Login from './components/pages/Login/Login.component';
import Registro from './components/pages/Registro/Registro.component';
import PrivateRoute from './components/UI/PrivateRoute';

/* Profesionales */
import ProfesionalDashboard from './components/pages/ProfesionalDashboard/ProfesionalDashboardContainer.component';
import SolicitudesDeTurno from './components/pages/ProfesionalDashboard/SolicitudesDeTurno/SolicitudesDeTurnoContainer.component';
import AgregarPaciente from './components/pages/ProfesionalDashboard/AgregarPaciente/AgregarPacienteContainer.component';
import PacienteInfo from './components/pages/ProfesionalDashboard/PacienteInfo/PacienteInfoContainer.component';

/* admin */
import ListaProfesionales from './components/pages/AdminDashboard/ListaProfesionales/ListaProfesionales.component';
import AutorizarProfesional from './components/pages/AdminDashboard/AutorizarProfesional/AutorizarProfesionalContainer.component';

if (localStorage.token) {
  /* set the token to the global default header of axios -> interceptor */
  setAuthToken(localStorage.token);
}

class App extends React.Component {
  componentDidMount() {
    const { loadUser } = this.props;
    loadUser();
  }

  render() {
    return (
      <BrowserRouter>
        <Navbar />
        <ToastContainer position="bottom-center" autoClose={3000} />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/dashboard/registro" component={Registro} />
          <Route exact path="/dashboard/login" component={Login} />
          <Route exact path="/dashboard" component={Dashboard} />
          {/* Admin routes */}
          <PrivateRoute
            exact
            path="/dashboard/admin/profesionales"
            component={ListaProfesionales}
            roles={['admin']}
          />
          <PrivateRoute
            exact
            path="/dashboard/admin/autorizarprofesional"
            component={AutorizarProfesional}
          />
          {/* profesional routes */}
          <PrivateRoute
            exact
            path="/dashboard/profesionales/solicitudesdeturno"
            component={SolicitudesDeTurno}
            roles={['profesional']}
          />
          <PrivateRoute
            exact
            path="/dashboard/profesionales/agregarpaciente"
            component={AgregarPaciente}
            roles={['profesional']}
          />
          <PrivateRoute
            exact
            path="/dashboard/profesionales/pacientes/:id"
            component={PacienteInfo}
            roles={['profesional']}
          />
          <PrivateRoute
            exact
            path="/dashboard/profesionales/:id"
            component={ProfesionalDashboard}
            roles={['profesional']}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default connect(
  null,
  { loadUser }
)(App);
