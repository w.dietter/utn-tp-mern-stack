import React from 'react';
import MasterHeader from './Header/MasterHeader.component';
import Section01 from './Sections/Section01.component';
import Section02 from './Sections/Section02.component';
import FloattingButton from '../../UI/Buttons/FloattingButton.component';
import { FaHome } from 'react-icons/fa';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.section01Ref = React.createRef();
    this.section02Ref = React.createRef();
    this.top = React.createRef();
  }

  handleScroll = ref => {
    ref.current.scrollIntoView({ behavior: 'smooth' });
  };

  render() {
    return (
      <div ref={this.top}>
        <span></span>
        <MasterHeader
          handleScroll={this.handleScroll}
          section01Ref={this.section01Ref}
          section02Ref={this.section02Ref}
        />
        <Section01 section01Ref={this.section01Ref} />
        <Section02 section02Ref={this.section02Ref} />
        <div onClick={() => this.handleScroll(this.top)}>
          <FloattingButton>
            <FaHome />
          </FloattingButton>
        </div>
      </div>
    );
  }
}
export default Home;
