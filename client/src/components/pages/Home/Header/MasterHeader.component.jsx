import React from 'react';
import Button from '../../../UI/Buttons/Button.component';

const MasterHeader = ({ handleScroll, section01Ref, section02Ref }) => {
  return (
    <header className="MasterHeader">
      <div className="MasterHeader__overlay">
        <div className="MasterHeader__container">
          <h1 className="MasterHeader__main-text slideInLeft">
            Intervenciones
          </h1>
          <h2 className="MasterHeader__secondary-text slideInLeft">
            Red de asistencia psi
          </h2>
          <div className="MasterHeader__buttons">
            <div onClick={() => handleScroll(section01Ref)}>
              <Button text="Quienes Somos" mainClass="MasterHeader__button" />
            </div>
            <div onClick={() => handleScroll(section02Ref)}>
              <Button
                text="Solicitar turno"
                mainClass="MasterHeader__button--light"
              />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default MasterHeader;
