import React from 'react';

const Section01 = ({ section01Ref }) => {
  return (
    <section ref={section01Ref} className="bg-primary py-5">
      <div className="container py-5">
        <div className="row mb-5">
          <div className="col-12 text-center">
            <h3 className="display-2">Quienes somos?</h3>
          </div>
        </div>
        <div className="row text-center">
          <div className="col-4">
            <h4 className="mb-3 display-1">Historia</h4>
            <p className="display-3">
              Intervensiones nació con la idea de poder brindar atención
              psicológica a personas que poseen patologías psicológicas,
              síntomas, preocupaciones o simplemente cualquier tipo de
              inquietud. Siendo nuestra principal motivación ayudar a nuestros
              pacientes a estar mejor por medio de un adecuado proceso
              terapéutico.
            </p>
          </div>
          <div className="col-4">
            <h4 className="mb-3 display-1">Nosotros</h4>
            <p className="display-3">
              Somos un grupo de psicólogos con amplia experiencia en el ámbito
              clínico, laboral y jurídico
            </p>
          </div>
          <div className="col-4">
            <h4 className="mb-3 display-1">Mision</h4>
            <p className="display-3">
              Nuestra trayectoria y recorrido personal nos permite abordar
              nuestro trabajo desde una perspectiva ética que se sostiene en la
              responsabilidad y la confiabilidad como ejes centrales de nuestro
              trabajo.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section01;
