import React from 'react';
import SolicitudTurno from '../SolicitudTurno.component';

const Section02 = ({ section02Ref }) => {
  return (
    <section
      ref={section02Ref}
      className="py-5 bg-secondary-light text-primary-dark"
    >
      <div className="container py-5">
        <SolicitudTurno />
      </div>
    </section>
  );
};

export default Section02;
