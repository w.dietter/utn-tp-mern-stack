import React from 'react';
import PropTypes from 'prop-types';

/* redux */
import { connect } from 'react-redux';
import { solicitarTurno } from '../../../redux/pacientes/pacientes.actions';

const initialState = {
  nombre: '',
  apellido: '',
  dni: '',
  edad: '',
  obraSocial: '',
  telefono: '',
  email: ''
};

class AddPacienteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState
    };
  }

  static propTypes = {
    solicitarTurno: PropTypes.func.isRequired
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { solicitarTurno } = this.props;
    solicitarTurno(this.state);
    this.setState({ ...initialState });
  };

  render() {
    const {
      nombre,
      apellido,
      dni,
      edad,
      obraSocial,
      telefono,
      email
    } = this.state;
    return (
      <div className="container p-5">
        <h3 className="text-center">
          Solicitar turno con nuestros profesionales
        </h3>
        <h4 className="display-4 mb-5 text-center">
          Completar el formulario para ser contactado a la brevedad
        </h4>
        <div className="row">
          <div className="col-10 col-sm-8 mx-auto">
            <form onSubmit={this.handleSubmit} className="container">
              <div className="row">
                {/* nombre */}
                <div className="col-12 col-sm-6">
                  <div className="form-group">
                    <label htmlFor="nombre">Nombre</label>
                    <input
                      type="text"
                      name="nombre"
                      value={nombre}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
                {/* apellido */}
                <div className="col-12 col-sm-6">
                  <div className="form-group">
                    <label htmlFor="apellido">Apellido</label>
                    <input
                      type="text"
                      name="apellido"
                      value={apellido}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                {/* dni */}
                <div className="col-12 col-sm-8">
                  <div className="form-group">
                    <label htmlFor="dni">Dni</label>
                    <input
                      type="text"
                      name="dni"
                      value={dni}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
                {/* edad */}
                <div className="col-12 col-sm-3 offset-sm-1">
                  <div className="form-group">
                    <label htmlFor="edad">Edad</label>
                    <input
                      type="text"
                      name="edad"
                      value={edad}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                {/* telefono */}
                <div className="col-12 col-sm-4">
                  <div className="form-group">
                    <label htmlFor="telefono">Telefono</label>
                    <input
                      type="text"
                      name="telefono"
                      value={telefono}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
                {/* email */}
                <div className="col-12 col-sm-8">
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      type="text"
                      name="email"
                      value={email}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              {/* obra social */}
              <div className="row">
                <div className="col-12 col-sm-12">
                  <div className="form-group">
                    <label htmlFor="obraSocial">Obra Social</label>
                    <input
                      type="text"
                      name="obraSocial"
                      value={obraSocial}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              {/* button */}
              <div className="text-center">
                <button className="Button btn btn-primary btn-lg shadow">
                  Enviar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { solicitarTurno }
)(AddPacienteForm);
