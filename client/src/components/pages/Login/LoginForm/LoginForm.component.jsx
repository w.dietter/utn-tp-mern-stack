import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { loginUser } from '../../../../redux/auth/auth.actions';

class LoginForm extends React.Component {
  state = {
    email: '',
    password: ''
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { loginUser, history } = this.props;
    loginUser(this.state, history);
    this.setState({
      email: '',
      password: ''
    });
  };

  render() {
    const { email, password } = this.state;
    return (
      <div className={`LoginForm container my-3`}>
        <div className="row">
          <div className="col-10 col-sm-6 mx-auto">
            <h3 className="text-center">Login</h3>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <button type="submit" className="btn btn-block">
                Enviar
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth: { isAuthenticated } }) => ({
  isAuthenticated
});

export default connect(
  mapStateToProps,
  { loginUser }
)(LoginForm);
