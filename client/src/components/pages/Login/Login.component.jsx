import React from 'react';
import LoginForm from './LoginForm/LoginForm.component';

const Login = props => (
  <div>
    <LoginForm {...props} />
  </div>
);

export default Login;
