import React from 'react';
import SolicitudesList from './SolicitudesList.component';
/* redux */
import { connect } from 'react-redux';
import { getSolicitudes } from '../../../../redux/pacientes/pacientes.actions';

class SolicitudesDeTurnoContainer extends React.Component {
  componentDidMount() {
    const { getSolicitudes } = this.props;
    getSolicitudes();
  }

  render() {
    const { solicitudes, isLoaded } = this.props;
    return (
      <div>
        <div className="container my-5">
          <h1 className="mb-5 text-center">Solicitudes de turno</h1>
          {solicitudes.length > 0 ? (
            <SolicitudesList isLoaded={isLoaded} solicitudes={solicitudes} />
          ) : (
            <div className="text-center display-3">
              No hay solicitudes nuevas
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ pacientes: { solicitudes, isLoaded } }) => ({
  solicitudes,
  isLoaded
});

export default connect(
  mapStateToProps,
  { getSolicitudes }
)(SolicitudesDeTurnoContainer);
