import React from 'react';
import withLoader from '../../../UI/withLoader/withLoader.component';
import SolicitudesItem from './SolicitudesItem.component';

const SolicitudesList = ({ solicitudes }) => {
  return (
    <ul className="row">
      {solicitudes.map(solicitud => (
        <SolicitudesItem key={solicitud._id} solicitud={solicitud} />
      ))}
    </ul>
  );
};

export default withLoader(SolicitudesList);
