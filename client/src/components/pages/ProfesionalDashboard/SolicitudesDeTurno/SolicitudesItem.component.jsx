import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { deleteSolicitudPaciente } from '../../../../redux/pacientes/pacientes.actions';
import { tomarPaciente } from '../../../../redux/profesionales/profesionales.actions';

const SolicitudesItem = ({
  solicitud: { nombre, apellido, email, telefono, _id: pacienteId },
  profesional: { _id: profesionalId },
  deleteSolicitudPaciente,
  tomarPaciente
}) => {
  const handleSolicitudDelete = pacienteId => {
    deleteSolicitudPaciente(pacienteId);
  };

  const handleTomarPaciente = (profesionalId, pacienteId) => {
    tomarPaciente(profesionalId, pacienteId);
  };

  return (
    <li className="col-12 col-sm-4 my-3">
      <div className="card bg-primary text-secondary-light shadow-sm">
        <div className="card-body">
          <h3 className="card-title">
            {nombre} {apellido}
          </h3>
          <p className="card-text">telefono: {telefono}</p>
          <p className="card-text">email: {email}</p>
          <div className="d-flex justify-content-between mt-5">
            <button
              className="btn btn-success shadow-sm"
              onClick={() => handleTomarPaciente(profesionalId, pacienteId)}
            >
              Tomar paciente
            </button>
            <button
              className="btn btn-danger shadow-sm"
              onClick={() => handleSolicitudDelete(pacienteId)}
            >
              Borrar
            </button>
          </div>
        </div>
      </div>
    </li>
  );
};

const mapStateToProps = ({ profesionales: { profesional } }) => ({
  profesional
});

export default connect(
  mapStateToProps,
  { deleteSolicitudPaciente, tomarPaciente }
)(SolicitudesItem);
