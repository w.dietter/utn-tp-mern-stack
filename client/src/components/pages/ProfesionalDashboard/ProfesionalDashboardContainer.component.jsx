import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { getProfesionalByUserId } from '../../../redux/profesionales/profesionales.actions';
import ProfesionalDashboardPacientes from './ProfesionalDashboardPacientes/ProfesionalDashboardPacientes.component';

class ProfesionalDashboardContainer extends React.Component {
  componentDidMount() {
    const {
      match: {
        params: { id }
      },
      getProfesionalByUserId
    } = this.props;
    getProfesionalByUserId(id);
  }

  render() {
    const {
      profesionales: { profesional, isLoaded }
    } = this.props;
    return (
      <div>
        <h1 className="text-center my-5">Lista de pacientes</h1>
        {profesional && (
          <ProfesionalDashboardPacientes
            isLoaded={isLoaded}
            profesional={profesional}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ profesionales }) => {
  return {
    profesionales
  };
};

export default connect(
  mapStateToProps,
  { getProfesionalByUserId }
)(ProfesionalDashboardContainer);
