import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { updatePaciente } from '../../../../redux/pacientes/pacientes.actions';

const initialState = {
  nombre: '',
  apellido: '',
  edad: '',
  dni: '',
  obraSocial: '',
  diagnostico: '',
  telefono: '',
  email: ''
};

class PacienteInfoEditar extends React.Component {
  state = {
    ...initialState
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      updatePaciente,
      paciente: { _id }
    } = this.props;
    updatePaciente(this.state, _id);
    this.setState({
      ...initialState
    });
  };

  render() {
    const {
      nombre,
      apellido,
      edad,
      dni,
      diagnostico,
      obraSocial,
      telefono,
      email
    } = this.state;
    const { paciente } = this.props;

    return (
      <form className="py-3" onSubmit={this.handleSubmit}>
        <div className="row">
          <div className="form-group col-12">
            Nombre
            <input
              type="text"
              className="form-control"
              value={nombre}
              name="nombre"
              placeholder={paciente.nombre}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Apellido
            <input
              type="text"
              className="form-control"
              value={apellido}
              name="apellido"
              placeholder={paciente.apellido}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Edad
            <input
              type="text"
              className="form-control"
              value={edad}
              name="edad"
              placeholder={paciente.edad}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            DNI
            <input
              type="text"
              className="form-control"
              value={dni}
              name="dni"
              placeholder={paciente.dni}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Diagnostico
            <input
              type="text"
              className="form-control"
              value={diagnostico}
              name="diagnostico"
              placeholder={paciente.diagnostico}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Obra Social
            <input
              type="text"
              className="form-control"
              value={obraSocial}
              name="obraSocial"
              placeholder={paciente.obraSocial}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Telefono
            <input
              type="text"
              className="form-control"
              value={telefono}
              name="telefono"
              placeholder={paciente.telefono}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-12">
            Email
            <input
              type="text"
              className="form-control"
              value={email}
              name="email"
              placeholder={paciente.email}
              onChange={this.handleChange}
            />
          </div>
          <div className="col-12">
            <button type="submit" className="btn btn-secondary-dark btn-block">
              Guardar
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default connect(
  null,
  { updatePaciente }
)(PacienteInfoEditar);
