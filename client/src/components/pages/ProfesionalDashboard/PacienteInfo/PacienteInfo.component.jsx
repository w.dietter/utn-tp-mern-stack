import React from 'react';
import withLoader from '../../../UI/withLoader/withLoader.component';
import { FaIdCardAlt } from 'react-icons/fa';
import PacienteInfoEditar from './PacienteInfoEditar.component';

const PacienteInfo = props => {
  const [editar, setEditar] = React.useState(false);
  const {
    paciente: {
      nombre,
      apellido,
      edad,
      dni,
      obraSocial,
      telefono,
      email,
      diagnostico
    }
  } = props;
  return (
    <div className="PacienteInfo container">
      <div className="row">
        <div className="col-sm-4">
          <div className="PacienteInfo__icon">
            <FaIdCardAlt />
          </div>
        </div>
        <div className="col-sm-8 PacienteInfo__header">
          <h1 className="PacienteInfo__title">
            {nombre} {apellido}
          </h1>
          <button
            className="PacienteInfo__button btn btn-secondary-dark"
            onClick={() => setEditar(!editar)}
          >
            Editar
          </button>
        </div>
      </div>
      <hr />
      <div className="row">
        <div className="col-sm-6">
          <div className="col-12 PacienteInfo__text">Edad: {edad}</div>
          <div className="col-12 PacienteInfo__text">DNI: {dni}</div>
          <div className="col-12 PacienteInfo__text">
            Diagnostico: {diagnostico}
          </div>
          <div className="col-12 PacienteInfo__text">
            Obra social: {obraSocial}
          </div>
          <div className="col-12 PacienteInfo__text">Telefono: {telefono}</div>
          <div className="col-12 PacienteInfo__text">Email: {email}</div>
        </div>
        <div className="col-sm-6">
          {editar && <PacienteInfoEditar paciente={props.paciente} />}
        </div>
      </div>
    </div>
  );
};

PacienteInfo.defaultProps = {
  paciente: {
    nombre: '',
    apellido: ''
  }
};

export default withLoader(PacienteInfo);
