import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { getPacienteById } from '../../../../redux/pacientes/pacientes.actions';
import PacienteInfo from './PacienteInfo.component';

class PacienteInfoContainer extends React.Component {
  componentDidMount() {
    const {
      getPacienteById,
      match: {
        params: { id }
      }
    } = this.props;
    getPacienteById(id);
  }

  render() {
    const { paciente, isLoaded } = this.props;
    return (
      <div>
        {paciente && <PacienteInfo paciente={paciente} isLoaded={isLoaded} />}
      </div>
    );
  }
}

const mapStateToProps = ({ pacientes: { paciente, isLoaded } }) => ({
  paciente,
  isLoaded
});

export default connect(
  mapStateToProps,
  { getPacienteById }
)(PacienteInfoContainer);
