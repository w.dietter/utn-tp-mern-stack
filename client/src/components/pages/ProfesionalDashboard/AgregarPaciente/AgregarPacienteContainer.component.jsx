import React from 'react';
import AddPacienteForm from './AddPacienteForm.component';

class AgregarPacienteContainer extends React.Component {
  render() {
    return (
      <div className="container py-5">
        <h1 className="text-center">Agregar paciente</h1>
        <AddPacienteForm />
      </div>
    );
  }
}

export default AgregarPacienteContainer;
