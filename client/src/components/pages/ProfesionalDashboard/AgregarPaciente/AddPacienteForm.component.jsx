import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { addPaciente } from '../../../../redux/pacientes/pacientes.actions';

const initialState = {
  nombre: '',
  apellido: '',
  dni: '',
  edad: '',
  diagnostico: '',
  obraSocial: '',
  telefono: '',
  email: '',
  profesional: ''
};

class AddPacienteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState
    };
  }

  static defaultProps = {
    profesionales: []
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { addPaciente, profesional: currentProfesional } = this.props;
    this.setState(
      {
        profesional: currentProfesional._id
      },
      () => {
        addPaciente(this.state);
        this.setState({ ...initialState });
      }
    );
  };

  render() {
    const {
      nombre,
      apellido,
      dni,
      edad,
      diagnostico,
      obraSocial,
      telefono,
      email
    } = this.state;
    const { profesional: currentProfesional, isLoaded } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-10 col-sm-8 mx-auto">
            <form onSubmit={this.handleSubmit} className="container">
              <div className="row">
                {/* nombre */}
                <div className="col-12 col-sm-6">
                  <div className="form-group">
                    <label htmlFor="nombre">Nombre</label>
                    <input
                      type="text"
                      name="nombre"
                      value={nombre}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
                {/* apellido */}
                <div className="col-12 col-sm-6">
                  <div className="form-group">
                    <label htmlFor="apellido">Apellido</label>
                    <input
                      type="text"
                      name="apellido"
                      value={apellido}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                {/* dni */}
                <div className="col-12 col-sm-8">
                  <div className="form-group">
                    <label htmlFor="dni">Dni</label>
                    <input
                      type="text"
                      name="dni"
                      value={dni}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
                {/* edad */}
                <div className="col-12 col-sm-4">
                  <div className="form-group">
                    <label htmlFor="edad">Edad</label>
                    <input
                      type="text"
                      name="edad"
                      value={edad}
                      className="form-control"
                      onChange={this.handleChange}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              {/* profesional */}
              {isLoaded && currentProfesional && (
                <div className="form-group">
                  <label htmlFor="profesional">Profesional</label>
                  <input
                    type="text"
                    name="profesional"
                    onChange={this.handleChange}
                    className="form-control"
                    placeholder={`${currentProfesional.nombre} ${currentProfesional.apellido} / ${currentProfesional.especialidad}`}
                    disabled
                  />
                </div>
              )}
              {/* diagnostico */}
              <div className="form-group">
                <label htmlFor="diagnostico">Diagnostico</label>
                <input
                  type="text"
                  name="diagnostico"
                  value={diagnostico}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* obra social */}
              <div className="form-group">
                <label htmlFor="obraSocial">Obra Social</label>
                <input
                  type="text"
                  name="obraSocial"
                  value={obraSocial}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* telefono */}
              <div className="form-group">
                <label htmlFor="telefono">Telefono</label>
                <input
                  type="text"
                  name="telefono"
                  value={telefono}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* email */}
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  name="email"
                  value={email}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className="text-center">
                <button className="btn btn-primary btn-block mt-4">
                  Agregar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ profesionales: { profesional, isLoaded } }) => ({
  profesional,
  isLoaded
});

export default connect(
  mapStateToProps,
  { addPaciente }
)(AddPacienteForm);
