import React from 'react';
import withLoader from '../../../UI/withLoader/withLoader.component';
import PacienteItem from './PacienteItem.component';

const ProfesionalDashboardPacientes = ({
  profesional: { pacientes, user }
}) => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-12 col-sm-9 mx-auto">
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Nombre y apellido</th>
                <th>Edad</th>
                <th>DNI</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {pacientes.length > 0 &&
                pacientes.map(paciente => (
                  <PacienteItem
                    key={paciente._id}
                    paciente={paciente}
                    user={user}
                  />
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

ProfesionalDashboardPacientes.defaultProps = {
  profesional: {
    nombre: '',
    apellido: '',
    nroMatricula: '',
    especialidad: '',
    pacientes: []
  }
};

export default withLoader(ProfesionalDashboardPacientes);
