import React from 'react';
import { FaInfoCircle, FaTrashAlt } from 'react-icons/fa';
import { Link } from 'react-router-dom';
/* redux */
import { connect } from 'react-redux';
import { deletePaciente } from '../../../../redux/pacientes/pacientes.actions';

const PacienteItem = ({
  paciente: { nombre, apellido, edad, dni, _id },
  deletePaciente,
  user
}) => {
  return (
    <tr>
      <td>
        {nombre} {apellido}
      </td>
      <td>{edad}</td>
      <td>{dni}</td>
      <td className="d-flex justify-content-center align-items-center h-100">
        <div>
          <Link to={`/dashboard/profesionales/pacientes/${_id}`}>
            <FaInfoCircle />
          </Link>
        </div>
        <div
          className="ml-3 text-danger"
          style={{ cursor: 'pointer' }}
          onClick={() => deletePaciente(_id, user)}
        >
          <FaTrashAlt />
        </div>
      </td>
    </tr>
  );
};

export default connect(
  null,
  { deletePaciente }
)(PacienteItem);
