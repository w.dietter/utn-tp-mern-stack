import React from 'react';

const Dashboard = () => {
  return (
    <div className="DashboardLanding">
      <div className="container">
        <div className="DashboardLanding__heading">
          <h1>Bienvenidos a Interversiones red psi</h1>
          <h6>Sección exclusiva para profesionales de la red</h6>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
