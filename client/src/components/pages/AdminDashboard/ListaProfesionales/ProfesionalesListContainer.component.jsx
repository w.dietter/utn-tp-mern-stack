import React from 'react';
import ProfesionalesList from './ProfesionalesList.component';

/* redux */
import { connect } from 'react-redux';
import { getProfesionales } from '../../../../redux/profesionales/profesionales.actions';

class ProfesionalesListContainer extends React.Component {
  componentDidMount() {
    const { getProfesionales } = this.props;
    getProfesionales();
  }

  render() {
    const { isLoaded, profesionales } = this.props;
    return (
      <div className="container">
        <h1>Profesionales</h1>
        <ProfesionalesList profesionales={profesionales} isLoaded={isLoaded} />
      </div>
    );
  }
}

const mapStateToProps = ({ profesionales: { isLoaded, profesionales } }) => ({
  profesionales,
  isLoaded
});

export default connect(
  mapStateToProps,
  { getProfesionales }
)(ProfesionalesListContainer);
