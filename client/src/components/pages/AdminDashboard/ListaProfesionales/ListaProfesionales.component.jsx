import React from 'react';
import ProfesionalesListContainer from './ProfesionalesListContainer.component';

const AltaNuevoProfesional = () => (
  <div className="container">
    <h1 className="text-center my-5">Admin Dashboard</h1>
    <ProfesionalesListContainer />
  </div>
);

export default AltaNuevoProfesional;
