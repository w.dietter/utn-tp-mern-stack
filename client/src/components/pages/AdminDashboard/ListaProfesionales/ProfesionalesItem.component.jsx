import React from 'react';
import PropTypes from 'prop-types';

/* redux */
import { connect } from 'react-redux';
import { deleteProfesional } from '../../../../redux/profesionales/profesionales.actions';

const ProfesionalesItem = ({
  profesional: { _id, nombre, apellido, nroMatricula, especialidad },
  deleteProfesional
}) => (
  <li>
    <div className="card my-5 text-primary">
      <div className="card-body">
        <div className="card-title">
          {nombre} {apellido}
        </div>
        <hr />
        <p>Matricula: {nroMatricula}</p>
        <p className="text-capitalize">Especialidad: {especialidad}</p>
      </div>
      <button
        className="btn btn-danger btn-blocl"
        onClick={() => deleteProfesional(_id)}
      >
        Borrar
      </button>
    </div>
  </li>
);

ProfesionalesItem.propTypes = {
  profesional: PropTypes.object.isRequired
};

export default connect(
  null,
  { deleteProfesional }
)(ProfesionalesItem);
