import React from 'react';
import ProfesionalesItem from './ProfesionalesItem.component';
import withLoader from '../../../UI/withLoader/withLoader.component';

const ProfesionalesList = ({ profesionales }) => {
  return (
    <ul>
      {profesionales.map(profesional => (
        <ProfesionalesItem key={profesional._id} profesional={profesional} />
      ))}
    </ul>
  );
};

export default withLoader(ProfesionalesList);
