import React from 'react';
import withLoader from '../../../UI/withLoader/withLoader.component';
import AutorizarProfesionalItem from './AutorizarProfesionalItem.component';

const AutorizarProfesionalList = ({ solicitudes }) => {
  return (
    <ul className="row">
      {solicitudes.map(solicitud => (
        <AutorizarProfesionalItem solicitud={solicitud} key={solicitud._id} />
      ))}
    </ul>
  );
};

export default withLoader(AutorizarProfesionalList);
