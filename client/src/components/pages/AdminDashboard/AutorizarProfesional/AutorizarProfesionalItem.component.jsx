import React from 'react';

/* redux */
import { connect } from 'react-redux';
import {
  aprobarSolicitudProfesional,
  deleteSolicitudProfesional
} from '../../../../redux/profesionales/profesionales.actions';

const AutorizarProfesionalItem = ({
  solicitud: {
    nombre,
    apellido,
    nroMatricula,
    especialidad,
    user: { email, role, name, _id: userId }
  },
  aprobarSolicitudProfesional,
  deleteSolicitudProfesional
}) => {
  const handleAprobarSolicitud = userId => {
    aprobarSolicitudProfesional(userId);
  };

  const handleDeleteSolicitud = userId => {
    deleteSolicitudProfesional(userId);
  };
  return (
    <li className="col-12 col-sm-4">
      <div className="card text-primary-dark">
        <div className="card-body">
          <div className="card-title">{nombre + ' ' + apellido}</div>
          <hr />
          <p className="card-text">Especialidad: {especialidad}</p>
          <p className="card-text">Número de matricula: {nroMatricula}</p>
          <p className="card-text">Email: {email}</p>
          <p className="card-text">Username: {name}</p>
          <p className="card-text">Rol: {role}</p>
          <div className="d-flex justify-content-around mt-5">
            <button
              className="btn btn-success"
              onClick={() => handleAprobarSolicitud(userId)}
            >
              Aprobar
            </button>
            <button
              className="btn btn-danger"
              onClick={() => handleDeleteSolicitud(userId)}
            >
              Eliminar
            </button>
          </div>
        </div>
      </div>
    </li>
  );
};

export default connect(
  null,
  { aprobarSolicitudProfesional, deleteSolicitudProfesional }
)(AutorizarProfesionalItem);
