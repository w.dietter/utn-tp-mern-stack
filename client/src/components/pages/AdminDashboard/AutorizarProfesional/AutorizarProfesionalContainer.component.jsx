import React from 'react';
/* redux */
import { connect } from 'react-redux';
import { getSolicitudesAutorizacion } from '../../../../redux/profesionales/profesionales.actions';
import AutorizarProfesionalList from './AutorizarProfesionalList.component';

class AutorizarProfesionalContainer extends React.Component {
  componentDidMount() {
    const { getSolicitudesAutorizacion } = this.props;
    getSolicitudesAutorizacion();
  }

  render() {
    const { solicitudes, isLoaded } = this.props;
    return (
      <div className="container">
        <h1 className="text-center my-5">Autorizar profesional</h1>
        {solicitudes.length > 0 ? (
          <AutorizarProfesionalList
            isLoaded={isLoaded}
            solicitudes={solicitudes}
          />
        ) : (
          <div className="text-center display-3 mt-5 py-5">
            No hay solicitudes nuevas
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ profesionales: { solicitudes, isLoaded } }) => ({
  solicitudes,
  isLoaded
});

export default connect(
  mapStateToProps,
  { getSolicitudesAutorizacion }
)(AutorizarProfesionalContainer);
