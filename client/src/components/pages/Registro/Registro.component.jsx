import React from 'react';
import RegistroForm from './RegistroForm/RegistroForm';

const Registro = props => (
  <div>
    <RegistroForm {...props} />
  </div>
);

export default Registro;
