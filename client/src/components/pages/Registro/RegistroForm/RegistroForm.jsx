import React from 'react';

/* redux */
import { connect } from 'react-redux';
import { registerUser } from '../../../../redux/auth/auth.actions';

class RegistroForm extends React.Component {
  state = {
    name: '',
    nombre: '',
    apellido: '',
    especialidad: '',
    nroMatricula: '',
    email: '',
    password: ''
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { registerUser, history } = this.props;
    registerUser(this.state, history);
    this.setState({
      name: '',
      nombre: '',
      apellido: '',
      especialidad: '',
      nroMatricula: '',
      email: '',
      password: ''
    });
  };
  render() {
    const {
      name,
      nombre,
      especialidad,
      apellido,
      nroMatricula,
      email,
      password
    } = this.state;
    return (
      <div className={`RegistroForm container my-3 bg-primary-dark`}>
        <div className="row">
          <div className="col-12 col-sm-10 mx-auto">
            <h3 className="text-center">Registro</h3>
            <p className="text-center">
              Solicitud de alta de usuario para profesionales de la red.
            </p>
            <form onSubmit={this.handleSubmit}>
              {/* Username */}
              <div className="form-group">
                <label htmlFor="name">Username</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  value={name}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* nombre */}
              <div className="form-group">
                <label htmlFor="nombre">Nombre</label>
                <input
                  type="text"
                  name="nombre"
                  value={nombre}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* apellido */}
              <div className="form-group">
                <label htmlFor="apellido">Apellido</label>
                <input
                  type="text"
                  name="apellido"
                  value={apellido}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* especialidad */}
              <div className="form-group">
                <label htmlFor="especialidad">Especialidad</label>
                <select
                  className="custom-select"
                  name="especialidad"
                  value={especialidad}
                  onChange={this.handleChange}
                >
                  <option defaultValue>Seleccionar especialidad</option>
                  <option value="psiquiatra">Psiquiatra</option>
                  <option value="psicologo">Psicologo</option>
                </select>
              </div>
              {/* nroMatricula */}
              <div className="form-group">
                <label htmlFor="nroMatricula"> Numero de matricula: </label>
                <input
                  type="text"
                  name="nroMatricula"
                  value={nroMatricula}
                  className="form-control"
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* Email */}
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              {/* password */}
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className="text-center">
                <button
                  type="submit"
                  className="btn btn-lg btn-outline-secondary btn-block my-5"
                >
                  Enviar
                </button>
              </div>
            </form>
            <p className="text-center">
              El envio de la solicitud será procesado por el administrador del
              sitio.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { registerUser }
)(RegistroForm);
