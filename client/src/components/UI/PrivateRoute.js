import React from 'react';
import { Route, Redirect } from 'react-router-dom';
/* redux */
import { connect } from 'react-redux';

const PrivateRoute = ({
  component: Component,
  isAuthenticated,
  isLoaded,
  user,
  userRole,
  roles,
  ...otherProps
}) => {
  return (
    <Route
      {...otherProps}
      render={props => {
        if (!isAuthenticated && isLoaded) {
          return <Redirect to="/" />;
        }

        if (isLoaded && roles && roles.indexOf(user.role) === -1) {
          return <Redirect to="/" />;
        }

        return <Component {...props} />;
      }}
    />
  );
};

const mapStateToProps = ({ auth: { isAuthenticated, user, isLoaded } }) => ({
  isAuthenticated,
  user,
  isLoaded
});

export default connect(mapStateToProps)(PrivateRoute);
