import React from 'react';
import Loader from './Loader.component';

const withLoader = WrappedComponent => {
  return ({ isLoaded, ...otherProps }) => {
    if (!isLoaded) {
      return <Loader />;
    }

    return <WrappedComponent {...otherProps} />;
  };
};

export default withLoader;
