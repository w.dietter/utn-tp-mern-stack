import React from 'react';

const Loader = () => {
  return (
    <div className="Loader">
      <div className="Loader__item"></div>
      <div className="Loader__item"></div>
      <div className="Loader__item"></div>
    </div>
  );
};

export default Loader;
