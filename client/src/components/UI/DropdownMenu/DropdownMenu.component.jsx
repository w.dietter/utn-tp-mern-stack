import React from 'react';
import { FaArrowAltCircleDown } from 'react-icons/fa';

class DropdownMenu extends React.Component {
  state = {
    show: false
  };

  showDropdownMenu = e => {
    e.preventDefault();
    this.setState({ show: true }, () => {
      document.addEventListener('click', this.hideDropdownMenu);
    });
  };

  hideDropdownMenu = () => {
    this.setState({ show: false }, () => {
      document.removeEventListener('click', this.hideDropdownMenu);
    });
  };

  render() {
    const { text, children, classNames } = this.props;
    const { show } = this.state;
    return (
      <div className="DropdownMenu">
        <div
          onClick={this.showDropdownMenu}
          className={`${classNames} DropdownMenu__toggle`}
        >
          {text} <FaArrowAltCircleDown className="DropdownMenu__icon" />
        </div>
        <ul
          className={`DropdownMenu__content ${
            !show ? 'DropdownMenu--hide' : ''
          }`}
        >
          {children}
        </ul>
      </div>
    );
  }
}

export default DropdownMenu;
