import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ text, mainClass }) => {
  return <button className={`Button ${mainClass}`}>{text}</button>;
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  mainClass: PropTypes.string.isRequired
};

export default Button;
