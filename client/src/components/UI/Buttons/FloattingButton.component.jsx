import React from 'react';

const FloattingButton = ({ text, children }) => {
  return <button className="FloattingButton shadow">{children}</button>;
};

export default FloattingButton;
