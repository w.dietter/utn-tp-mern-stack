import React from 'react';
import { NavLink } from 'react-router-dom';
import DropdownMenu from '../UI/DropdownMenu/DropdownMenu.component';

const AdminLinks = () => (
  <>
    <li>
      <DropdownMenu text="Admin">
        <li className="DropdownMenu__item">
          <NavLink exact to="/dashboard/admin/profesionales">
            Lista de profesionales
          </NavLink>
        </li>
        <li className="DropdownMenu__item">
          <NavLink exact to="/dashboard/admin/autorizarprofesional">
            Autorizar pedido de alta de profesional
          </NavLink>
        </li>
      </DropdownMenu>
    </li>
  </>
);

export default AdminLinks;
