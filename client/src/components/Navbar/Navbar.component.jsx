import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import DashboardNavigation from './DashboardNavigation.component';

const Navbar = ({ location: { pathname } }) => {
  return (
    <nav className={`navbar`}>
      <span className="navbar__logo">
        <NavLink to="/dashboard">Intervensiones red psi</NavLink>
      </span>
      <ul className="navbar__nav">
        {pathname.includes('/dashboard') && <DashboardNavigation />}
      </ul>
    </nav>
  );
};

export default withRouter(Navbar);
