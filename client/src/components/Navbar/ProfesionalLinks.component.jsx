import React from 'react';
import { NavLink } from 'react-router-dom';
import DropdownMenu from '../UI/DropdownMenu/DropdownMenu.component';

class ProfesionalLinks extends React.Component {
  render() {
    const {
      user: { _id }
    } = this.props;
    return (
      <>
        <li>
          <DropdownMenu text="Profesionales">
            <li className="DropdownMenu__item">
              <NavLink exact to="/dashboard/profesionales/solicitudesdeturno">
                Solicitudes de turno
              </NavLink>
            </li>
            <li className="DropdownMenu__item">
              <NavLink exact to={`/dashboard/profesionales/${_id}`}>
                Mis Pacientes
              </NavLink>
            </li>
            <li className="DropdownMenu__item">
              <NavLink exact to={`/dashboard/profesionales/agregarpaciente`}>
                Agregar Paciente
              </NavLink>
            </li>
          </DropdownMenu>
        </li>
      </>
    );
  }
}

export default ProfesionalLinks;
