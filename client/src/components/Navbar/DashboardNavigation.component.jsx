import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
/* redux */
import { connect } from 'react-redux';
import { logoutUser } from '../../redux/auth/auth.actions';
/* components */
import AdminLinks from './AdminLinks.component';
import ProfesionalLinks from './ProfesionalLinks.component';

const DashboardNavigation = ({ logoutUser, user, isAuthenticated }) => {
  const validateRole = (user, role) => {
    if (user && user.role === role && user.isValidatedByAdmin) {
      return true;
    }
    return false;
  };
  return (
    <>
      <li className="text-primary">
        <div className="navbar__currentuser d-none d-sm-block">
          <div>{user && `nombre de usuario: ${user.name}`}</div>
          <div>{user && `email: ${user.email}`}</div>
          <div>{user && user.role}</div>
        </div>
      </li>
      {!isAuthenticated && !user ? (
        <>
          <li>
            <NavLink
              className="navbar__nav-link"
              exact
              to="/dashboard/registro"
              activeClassName="active"
            >
              Registro
            </NavLink>
          </li>
          <li>
            <NavLink
              className="navbar__nav-link"
              exact
              to="/dashboard/login"
              activeClassName="active"
            >
              Log in
            </NavLink>
          </li>
          )}
        </>
      ) : (
        <>
          {validateRole(user, 'profesional') ? (
            <ProfesionalLinks user={user} />
          ) : null}
          {validateRole(user, 'admin') ? <AdminLinks /> : null}

          <li onClick={logoutUser}>
            <NavLink className="navbar__nav-link" to="/">
              Log out
            </NavLink>
          </li>
        </>
      )}
    </>
  );
};

const mapStateToProps = ({ auth: { user } }) => ({
  user
});

export default withRouter(
  connect(
    mapStateToProps,
    { logoutUser }
  )(DashboardNavigation)
);
