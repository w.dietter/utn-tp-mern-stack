import { UsersActionTypes } from '../types';

const initialState = {
  users: [],
  isLoaded: true,
  error: null
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case UsersActionTypes.USERS_FETCH_START:
      return {
        ...state,
        isLoaded: false
      };
    case UsersActionTypes.USERS_FETCH_ERROR:
      return {
        ...state,
        isLoaded: true
      };
    case UsersActionTypes.GET_USERS_SUCCESS:
      return {
        ...state,
        users: payload,
        isLoaded: true
      };
    default:
      return state;
  }
};
