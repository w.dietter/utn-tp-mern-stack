import { UsersActionTypes } from '../types';
import axios from 'axios';

export const getUsers = () => async dispatch => {
  dispatch({
    type: UsersActionTypes.USERS_FETCH_START
  });
  try {
    const res = await axios.get('/api/users');
    dispatch({
      type: UsersActionTypes.GET_USERS_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: UsersActionTypes.USERS_FETCH_ERROR,
      payload: error.message
    });
  }
};

export const filterUsers = (text) => async dispatch => {
  
}