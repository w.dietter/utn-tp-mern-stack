import axios from 'axios';
import { PacienteActionTypes } from '../types';
import { toast } from 'react-toastify';
import { getProfesionalByUserId } from '../profesionales/profesionales.actions';

const axiosConfig = {
  headers: {
    'Content-Type': 'application/json'
  }
};

/* Get all pacientes -> GET /api/pacientes */
export const getPacientes = () => async dispatch => {
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.get('/api/pacientes');
    dispatch({
      type: PacienteActionTypes.GET_PACIENTES_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* GET paciente by id -> GET /api/pacientes/:id */
export const getPacienteById = pacienteId => async dispatch => {
  const url = `/api/pacientes/${pacienteId}`;
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.get(url);
    dispatch({
      type: PacienteActionTypes.GET_PACIENTE_BYID,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* Add Paciente -> POST  /api/pacientes */
export const addPaciente = paciente => async dispatch => {
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const body = JSON.stringify({ ...paciente });
    const res = await axios.post('/api/pacientes', body, axiosConfig);
    dispatch({
      type: PacienteActionTypes.ADD_PACIENTE_SUCCESS,
      payload: res.data
    });
    toast.success('Paciente agregado exitosamente.');
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* Solicitar Turno -> POST /api/pacientes/solicitarturno */
export const solicitarTurno = paciente => async dispatch => {
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const body = JSON.stringify({ ...paciente });
    const res = await axios.post(
      '/api/pacientes/solicitarturno',
      body,
      axiosConfig
    );
    dispatch({
      type: PacienteActionTypes.SOLICITAR_TURNO_PACIENTE_SUCCESS
    });
    toast.success(res.data.msg);
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* GET solicitudes (pacientes sin profesional) -> GET api/pacientes/solicitudes */
export const getSolicitudes = () => async dispatch => {
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.get('/api/pacientes/solicitudes');
    dispatch({
      type: PacienteActionTypes.GET_PACIENTES_SOLICITUDES_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error
    });
  }
};

/* Delete solicitudes -> DELETE /api/pacientes/solicitudes/:id */
export const deleteSolicitudPaciente = pacienteId => async dispatch => {
  const url = `/api/pacientes/solicitudes/${pacienteId}`;
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.delete(url);
    dispatch({
      type: PacienteActionTypes.DELETE_SOLICITUD_PACIENTE,
      payload: pacienteId
    });
    toast.success(res.data.msg);
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error
    });
  }
};

/* Delete paciente -> DELETE /api/pacientes/:id */
export const deletePaciente = (id, profesionalId) => async dispatch => {
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.delete(`/api/pacientes/${id}`);
    dispatch({
      type: PacienteActionTypes.DELETE_PACIENTE_SUCCESS,
      payload: id
    });
    toast.success(res.data.msg);
    dispatch(getProfesionalByUserId(profesionalId));
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* PUT update paciente -> PUT /api/pacientes/:id */
export const updatePaciente = (paciente, pacienteId) => async dispatch => {
  const url = `/api/pacientes/${pacienteId}`;
  const body = JSON.stringify({ ...paciente });
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.put(url, body, axiosConfig);
    dispatch({
      type: PacienteActionTypes.UPDATE_PACIENTE,
      payload: res.data
    });
    toast.success('Paciente editado con exito.');
  } catch (error) {
    dispatch({
      type: PacienteActionTypes.PACIENTES_FETCH_ERROR,
      payload: error.message
    });
  }
};
