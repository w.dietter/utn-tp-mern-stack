import { PacienteActionTypes } from '../types';

const initialState = {
  pacientes: [],
  paciente: null,
  solicitudes: [],
  isLoaded: true,
  error: null
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case PacienteActionTypes.PACIENTES_FETCH_START: {
      return {
        ...state,
        isLoaded: false
      };
    }
    case PacienteActionTypes.GET_PACIENTES_SUCCESS:
      return {
        ...state,
        pacientes: payload,
        isLoaded: true
      };
    case PacienteActionTypes.GET_PACIENTE_BYID:
    case PacienteActionTypes.UPDATE_PACIENTE:
      return {
        ...state,
        paciente: payload,
        isLoaded: true
      };
    case PacienteActionTypes.GET_PACIENTES_SOLICITUDES_SUCCESS:
      return {
        ...state,
        solicitudes: payload,
        isLoaded: true
      };
    case PacienteActionTypes.DELETE_SOLICITUD_PACIENTE:
    case PacienteActionTypes.PACIENTE_TOMADO_POR_PROFESIONAL:
      return {
        ...state,
        solicitudes: state.solicitudes.filter(
          solicitud => solicitud._id !== payload
        ),
        isLoaded: true
      };
    case PacienteActionTypes.ADD_PACIENTE_SUCCESS:
      return {
        ...state,
        pacientes: [...state.pacientes, payload],
        isLoaded: true
      };
    case PacienteActionTypes.SOLICITAR_TURNO_PACIENTE_SUCCESS:
      return {
        ...state,
        isLoaded: true
      };
    case PacienteActionTypes.PACIENTES_FETCH_ERROR:
      return {
        ...state,
        isLoaded: true,
        error: payload
      };
    case PacienteActionTypes.DELETE_PACIENTE_SUCCESS:
      return {
        ...state,
        pacientes: state.pacientes.filter(paciente => paciente._id !== payload),
        isLoaded: true
      };
    default:
      return state;
  }
};
