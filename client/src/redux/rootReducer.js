import { combineReducers } from 'redux';
import authReducer from './auth/auth.reducer';
import pacientesReducer from './pacientes/pacientes.reducer';
import profesionalesReducer from './profesionales/profesionales.reducer';
import usersReducer from './users/users.reducer';

export default combineReducers({
  auth: authReducer,
  pacientes: pacientesReducer,
  profesionales: profesionalesReducer,
  users: usersReducer
});
