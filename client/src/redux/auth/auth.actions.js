import { AuthActionTypes } from '../types';
import axios from 'axios';
import setAuthToken from '../../utils/sethAuthToken';
import { toast } from 'react-toastify';
import { getProfesionalByUserId } from '../profesionales/profesionales.actions';

const axiosConfig = {
  headers: {
    'Content-Type': 'application/json'
  }
};

/* Load User */
export const loadUser = () => async dispatch => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }
  try {
    const res = await axios.get('/api/auth');
    dispatch({
      type: AuthActionTypes.USER_LOADED,
      payload: res.data
    });
    if (res.data.role === 'profesional' && res.data.isValidatedByAdmin) {
      dispatch(getProfesionalByUserId(res.data._id));
    }
  } catch (error) {
    dispatch({
      type: AuthActionTypes.AUTH_ERROR
    });
  }
};

/* Register User */
export const registerUser = (user, history) => async dispatch => {
  const body = JSON.stringify({ ...user });
  try {
    const res = await axios.post('/api/users', body, axiosConfig);
    dispatch({
      type: AuthActionTypes.REGISTER_SUCCESS
    });
    toast.success(res.data.msg);
    history.push('/dashboard');
  } catch (error) {
    dispatch({
      type: AuthActionTypes.REGISTER_FAIL
    });
  }
};

export const loginUser = (user, history) => async dispatch => {
  const body = JSON.stringify({ ...user });
  try {
    const res = await axios.post('/api/auth', body, axiosConfig);
    dispatch({
      type: AuthActionTypes.LOGIN_SUCCESS,
      payload: res.data
    });
    dispatch(loadUser());
    toast.success(`Bienvenido`);
    history.push('/dashboard');
  } catch (error) {
    const { msg } = error.response.data;
    dispatch({
      type: AuthActionTypes.LOGIN_FAIL
    });
    toast.error(msg);
  }
};

export const logoutUser = () => ({
  type: AuthActionTypes.LOGOUT
});
