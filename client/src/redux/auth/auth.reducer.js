import { AuthActionTypes } from '../types';

const initialState = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  isLoaded: false,
  user: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case AuthActionTypes.REGISTER_SUCCESS:
      return state;
    case AuthActionTypes.LOGIN_SUCCESS:
      localStorage.setItem('token', payload.token);
      return {
        ...state,
        isLoaded: true,
        isAuthenticated: true
      };
    case AuthActionTypes.USER_LOADED:
      console.log(payload);
      return {
        ...state,
        isAuthenticated: true,
        user: payload,
        isLoaded: true
      };
    // case AuthActionTypes.AUTH_ERROR:
    case AuthActionTypes.REGISTER_FAIL:
    case AuthActionTypes.LOGIN_FAIL:
    case AuthActionTypes.LOGOUT:
      localStorage.removeItem('token');
      return {
        token: null,
        isAuthenticated: false,
        user: null,
        isLoaded: true
      };
    default:
      return state;
  }
}
