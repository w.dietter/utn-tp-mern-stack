import { ProfesionalesActionTypes, PacienteActionTypes } from '../types';
import axios from 'axios';
import { toast } from 'react-toastify';

const axiosConfig = {
  headers: {
    'Content-Type': 'application/json'
  }
};

/* get all profesionales -> GET /api/profesionales */
export const getProfesionales = () => async dispatch => {
  try {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
    });
    const res = await axios.get('/api/profesionales');
    dispatch({
      type: ProfesionalesActionTypes.GET_PROFESIONALES_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR
    });
  }
};

/* GET profesional by id -> GET api/profesionales/:id */
export const getProfesionalByUserId = userId => async dispatch => {
  const url = `/api/profesionales/${userId}`;
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const res = await axios.get(url);
    dispatch({
      type: ProfesionalesActionTypes.GET_PROFESIONAL_BY_USERID,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR
    });
  }
};

/* PUT tomar paciente -> PUT api/profesionales/:id */
export const tomarPaciente = (profesionalId, pacienteId) => async dispatch => {
  const url = `/api/profesionales/${profesionalId}/pacientes/${pacienteId}`;
  dispatch({
    type: PacienteActionTypes.PACIENTES_FETCH_START
  });
  try {
    const res = await axios.put(url);
    dispatch({
      type: PacienteActionTypes.PACIENTE_TOMADO_POR_PROFESIONAL,
      payload: pacienteId
    });
    toast.success(res.data.msg);
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR
    });
  }
};

/* GET solicitudes -> GET api/users/solicitudes */
export const getSolicitudesAutorizacion = () => async dispatch => {
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const res = await axios.get('/api/users/solicitudes');
    dispatch({
      type: ProfesionalesActionTypes.GET_PROFESIONALES_SOLICITUDES_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR
    });
  }
};

/* Add profesional -> POST /api/profesionales */
export const addProfesional = profesional => async dispatch => {
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const body = JSON.stringify({ ...profesional });
    const res = await axios.post('/api/profesionales', body, axiosConfig);
    dispatch({
      type: ProfesionalesActionTypes.ADD_PROFESIONAL_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* delete profesional -> DELETE /api/profesionales/:id */
export const deleteProfesional = id => async dispatch => {
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const res = await axios.delete(`/api/profesionales/${id}`);
    dispatch({
      type: ProfesionalesActionTypes.DELETE_PROFESIONAL_SUCCESS,
      payload: id
    });
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* PUT aprobar solicitud -> PUT api/users/solicitudes/:id */
export const aprobarSolicitudProfesional = userId => async dispatch => {
  const url = `/api/users/solicitudes/${userId}`;
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const res = await axios.put(url);
    dispatch({
      type: ProfesionalesActionTypes.APROBAR_SOLICITUD_PROFESIONAL,
      payload: userId
    });
    toast.success(res.data.msg);
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR,
      payload: error.message
    });
  }
};

/* DELETE solicitud paciente -> DELETE api/pacientes/solicitudes/:id */
export const deleteSolicitudProfesional = userId => async dispatch => {
  const url = `/api/users/solicitudes/${userId}`;
  dispatch({
    type: ProfesionalesActionTypes.PROFESIONALES_FETCH_START
  });
  try {
    const res = await axios.delete(url);
    dispatch({
      type: ProfesionalesActionTypes.DELETE_SOLICITUD_PROFESIONAL,
      payload: userId
    });
    toast.success(res.data.msg);
  } catch (error) {
    dispatch({
      type: ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR,
      payload: error.message
    });
  }
};
