import { ProfesionalesActionTypes } from '../types';

const initialState = {
  profesionales: [],
  profesional: null,
  solicitudes: [],
  isLoaded: true,
  error: null
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case ProfesionalesActionTypes.PROFESIONALES_FETCH_START:
      return {
        ...state,
        isLoaded: false
      };
    case ProfesionalesActionTypes.PROFESIONALES_FETCH_ERROR:
      return {
        ...state,
        error: payload,
        isLoaded: true
      };
    case ProfesionalesActionTypes.GET_PROFESIONALES_SUCCESS:
      return {
        ...state,
        profesionales: payload,
        isLoaded: true
      };
    case ProfesionalesActionTypes.GET_PROFESIONALES_SOLICITUDES_SUCCESS:
      return {
        ...state,
        solicitudes: payload,
        isLoaded: true
      };
    case ProfesionalesActionTypes.APROBAR_SOLICITUD_PROFESIONAL:
    case ProfesionalesActionTypes.DELETE_SOLICITUD_PROFESIONAL:
      return {
        ...state,
        solicitudes: state.solicitudes.filter(
          solicitud => solicitud.user._id !== payload
        ),
        isLoaded: true
      };
    case ProfesionalesActionTypes.GET_PROFESIONAL_BY_USERID:
      return {
        ...state,
        profesional: payload,
        isLoaded: true
      };
    case ProfesionalesActionTypes.ADD_PROFESIONAL_SUCCESS:
      return {
        ...state,
        profesionales: [...state.profesionales, payload],
        isLoaded: true
      };
    case ProfesionalesActionTypes.DELETE_PROFESIONAL_SUCCESS:
      return {
        ...state,
        profesionales: state.profesionales.filter(
          profesional => profesional._id !== payload
        ),
        isLoaded: true
      };
    default:
      return state;
  }
};
