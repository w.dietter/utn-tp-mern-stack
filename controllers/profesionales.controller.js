const mongoose = require('mongoose');
const Profesional = require('../models/Profesional.model');
const Paciente = require('../models/Paciente.model');
const User = require('../models/User.model');

exports.getProfesionales = async (req, res) => {
  try {
    const profesionales = await Profesional.find().populate(
      'pacientes',
      null,
      'paciente'
    );
    return res.status(200).json(profesionales);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.getProfesionalByUserId = async (req, res) => {
  try {
    const { id } = req.params;
    const profesional = await Profesional.findOne({
      user: mongoose.Types.ObjectId(id)
    }).populate('pacientes', null, 'paciente');

    if (!profesional) {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    return res.status(200).json(profesional);
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.createProfesional = async (req, res) => {
  try {
    const { nombre, apellido, nroMatricula, especialidad, user } = req.body;
    let profesional = await Profesional.findOne({ nroMatricula });

    if (profesional) {
      return res.status(400).json({ msg: 'Profesional already exists.' });
    }

    profesional = new Profesional();
    /* add user id to user fiel in profesional model */
    if (user) profesional.user = user;
    if (nombre) profesional.nombre = nombre;
    if (apellido) profesional.apellido = apellido;
    if (nroMatricula) profesional.nroMatricula = nroMatricula;
    if (especialidad) profesional.especialidad = especialidad;
    await profesional.save();

    /* Update user role to profesional */
    const updatedUser = await User.findById(user);
    updatedUser.role = 'profesional';
    await updatedUser.save();
    /* return the created profesional to the front end. */
    return res.status(200).json(profesional);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.deleteProfesional = async (req, res) => {
  try {
    const { id } = req.params;
    const profesional = await Profesional.findById(id);
    if (!profesional) {
      return res.status(400).json({ msg: 'Not found.' });
    }
    await profesional.remove();
    return res.status(200).json({ msg: 'Profesional deleted.' });
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.updateProfesional = async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre, apellido, nroMatricula, especialidad } = req.body;
    const profesional = await Profesional.findById(id);
    if (!profesional) {
      return res.status(400).json({ msg: 'Profesional not found' });
    }

    if (nombre) profesional.nombre = nombre;
    if (apellido) profesional.apellido = apellido;
    if (nroMatricula) profesional.nroMatricula = nroMatricula;
    if (especialidad) profesional.especialidad = especialidad;

    await profesional.save();
    return res.status(200).json({ msg: 'Profesional updated.' });
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.addPacienteToProfesional = async (req, res) => {
  try {
    const { profesionalId, pacienteId } = req.params;
    const profesional = await Profesional.findById(profesionalId);
    if (!profesional) {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    const paciente = await Paciente.findById(pacienteId);
    if (!paciente) {
      return res.status(400).json({ msg: 'Paciente not found' });
    }
    profesional.pacientes.push(pacienteId);
    paciente.profesional = mongoose.Types.ObjectId(profesionalId);
    await profesional.save();
    await paciente.save();

    return res.status(200).json({
      msg: `Paciente tomado en tratamiento por ${profesional.nombre} ${profesional.apellido}`
    });
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};
