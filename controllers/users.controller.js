const mongoose = require('mongoose');
const User = require('../models/User.model');
const Profesional = require('../models/Profesional.model');

/*@route 			POST			api/users
  @desc 			Register user and return token
  @access 		Public		*/
exports.registerUser = async (req, res) => {
  const {
    name,
    email,
    password,
    nombre,
    apellido,
    nroMatricula,
    especialidad
  } = req.body;

  try {
    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ msg: 'User already exists.' });
    }
    const hashedPassword = await User.hashPassword(password);
    user = new User({ name, email, password: hashedPassword });
    await user.save();

    const profesional = new Profesional({
      user: user._id,
      nombre,
      apellido,
      nroMatricula,
      especialidad
    });
    await profesional.save();
    return res.status(200).json({
      msg:
        'Usuario creado, la petición sera revisada por el adminsitrador a la brevedad'
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send('Server Error');
  }
};

exports.getUsers = async (req, res) => {
  try {
    const users = await User.find().select('-password');
    return res.status(200).json(users);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

exports.getSolicitudes = async (req, res) => {
  try {
    const users = await Profesional.getNotValidatedByAdmin();
    return res.status(200).json(users);
  } catch (error) {
    console.error(error);
    return res.status(500).send('Server Error');
  }
};

exports.aprobarSolicitud = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await User.findById(id);
    if (!user) {
      return res.status(400).json({ msg: 'User not found' });
    }
    user.isValidatedByAdmin = true;
    await user.save();
    return res.status(200).json({ msg: 'Solicitud aprobada exitosamente.' });
  } catch (error) {
    console.error(error);
    return res.status(500).send('Server Error');
  }
};

exports.deleteSolicitud = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await User.findById(id);
    const profesional = await Profesional.findOne({
      user: { _id: mongoose.Types.ObjectId(id) }
    });
    if (user && profesional) {
      await user.remove();
      await profesional.remove();
    }
    return res.status(200).json({ msg: 'Solicitud borrada exitosamente' });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};
