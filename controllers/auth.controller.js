const config = require('config');
const bcrypt = require('bcryptjs');
const User = require('../models/User.model');
const generateAuthToken = require('../utils/generateAuthToken');

/*@route 			GET			/api/auth/
  @desc 			Get auth user providing the token -> check if the token is valid ('x-access-token').
  @access 		PRIVATE		*/
exports.authenticateUser = async (req, res) => {
  const { id } = req.user;
  try {
    const user = await User.findById(id).select('-password');
    return res.status(200).json(user);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/*@route 			POST			/api/auth/
  @desc 			Authenticate user and get token -> Log in
  @access 		Public		*/
exports.loginUser = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ msg: 'Datos incorrectos.' });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ msg: 'Datos incorrectos.' });
    }

    if (!user.isValidatedByAdmin) {
      return res.status(401).json({
        msg:
          'El usuario no esta validado por el administrador. Comunicarse con el administrador del sitio para solicitar autorización.'
      });
    }

    const payload = {
      user: {
        id: user.id
      }
    };

    const token = await generateAuthToken(payload, config.get('jwtSecret'));

    return res.status(200).json({ token });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};
