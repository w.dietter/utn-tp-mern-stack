const Paciente = require('../models/Paciente.model');
const Profesional = require('../models/Profesional.model');

/* api/pacientes */
exports.getPacientes = async (req, res) => {
  try {
    const pacientes = await Paciente.find()
      .populate('profesional')
      .select('-pacientes');

    if (pacientes.length === 0) {
      return res.status(400).json({ msg: 'Pacientes not found' });
    }
    return res.json(pacientes);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes/:id */
exports.getPacienteById = async (req, res) => {
  try {
    const { id } = req.params;
    const paciente = await Paciente.findById(id)
      .populate('profesional')
      .select('-pacientes');
    if (!paciente) {
      return res.status(400).json({ msg: 'Paciente not found' });
    }
    return res.status(200).json(paciente);
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes/solicitudes */
exports.getSolicitudes = async (req, res) => {
  try {
    const pacientes = await Paciente.find({ profesional: null });
    return res.status(200).json(pacientes);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes/solicitudes/:id */
exports.deleteSolicitudPaciente = async (req, res) => {
  try {
    const { id } = req.params;
    const paciente = await Paciente.findByIdAndDelete(id);
    return res.status(200).json({ msg: 'Solicitud borrada exitosamente' });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/solicitarturno */
exports.solicitarTurno = async (req, res) => {
  try {
    const {
      nombre,
      apellido,
      dni,
      edad,
      obraSocial,
      telefono,
      email
    } = req.body;

    let paciente = await Paciente.findOne({ dni });
    if (paciente) {
      return res.status(400).json({ msg: 'Paciente already exists' });
    }

    paciente = new Paciente();
    if (nombre) paciente.nombre = nombre;
    if (apellido) paciente.apellido = apellido;
    if (dni) paciente.dni = dni;
    if (edad) paciente.edad = edad;
    if (obraSocial) paciente.obraSocial = obraSocial;
    if (email) paciente.email = email;
    if (telefono) paciente.telefono = telefono;
    await paciente.save();
    return res.status(200).json({ msg: 'Turno solicitado exitosamente' });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes */
exports.createPaciente = async (req, res) => {
  try {
    const {
      profesional,
      nombre,
      apellido,
      dni,
      edad,
      diagnostico,
      obraSocial,
      telefono,
      email
    } = req.body;

    let paciente = await Paciente.findOne({ dni });
    if (paciente) {
      return res.status(400).json({ msg: 'Paciente already exists' });
    }

    paciente = new Paciente();
    if (profesional) paciente.profesional = profesional;
    if (nombre) paciente.nombre = nombre;
    if (apellido) paciente.apellido = apellido;
    if (dni) paciente.dni = dni;
    if (edad) paciente.edad = edad;
    if (diagnostico) paciente.diagnostico = diagnostico;
    if (obraSocial) paciente.obraSocial = obraSocial;
    if (email) paciente.email = email;
    if (telefono) paciente.telefono = telefono;
    await paciente.save();

    const profesionalDoc = await Profesional.findById(profesional);
    profesionalDoc.pacientes.push(paciente._id);
    await profesionalDoc.save();
    /* return the new paciente with the profesional field populated */
    paciente = await Paciente.findById(paciente._id)
      .populate('profesional')
      .select('-pacientes');
    return res.status(200).json(paciente);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes/:id */
exports.deletePaciente = async (req, res) => {
  try {
    const { id } = req.params;
    const paciente = await Paciente.findById(id);
    if (!paciente) {
      return res.status(400).json({ msg: 'Paciente not found' });
    }
    await paciente.remove();
    return res.status(200).json({ msg: 'Paciente deleted' });
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Profesional not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};

/* api/pacientes/:id */
exports.updatePaciente = async (req, res) => {
  try {
    const {
      nombre,
      apellido,
      dni,
      edad,
      diagnostico,
      obraSocial,
      telefono,
      email
    } = req.body;
    const { id } = req.params;
    const paciente = await Paciente.findById(id);

    if (nombre) paciente.nombre = nombre;
    if (apellido) paciente.apellido = apellido;
    if (dni) paciente.dni = dni;
    if (edad) paciente.edad = edad;
    if (diagnostico) paciente.diagnostico = diagnostico;
    if (obraSocial) paciente.obraSocial = obraSocial;
    if (email) paciente.email = email;
    if (telefono) paciente.telefono = telefono;

    await paciente.save();
    return res.status(200).json(paciente);
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Paciente not found' });
    }
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
};
