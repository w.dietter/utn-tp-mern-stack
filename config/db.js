const mongoose = require('mongoose');
const config = require('config');
const chalk = require('chalk');

const db = config.get('mongoURI');
// const db = config.get('mongoLOCAL');
const connectDB = async () => {
  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log(chalk.bgRed('MongoDB connected.'));
  } catch (error) {
    process.exit(1);
  }
};

module.exports = connectDB;
