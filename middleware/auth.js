const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
  const token = req.header('x-access-token');
  if (!token) {
    return res.status(401).json({ msg: 'No token.' });
  }

  try {
    const decoded = jwt.decode(token, config.get('jwtSecret'));
    req.user = decoded.user;
    next();
  } catch (error) {
    return res.status(401).send('Invalid Token.');
  }
};
